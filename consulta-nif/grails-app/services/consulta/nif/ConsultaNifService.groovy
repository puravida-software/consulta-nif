package consulta.nif

import groovy.util.logging.Log
import wslite.soap.*

import javax.annotation.PostConstruct

@Log
class ConsultaNifService {

    private SOAPClient client

    private static String vnif =
            "http://www2.agenciatributaria.gob.es/static_files/common/internet/dep/aplicaciones/es/aeat/burt/jdit/ws/VNifV2Ent.xsd";

    @PostConstruct
    void init(){
        client = new SOAPClient('https://www1.agenciatributaria.gob.es/wlpl/BURT-JDIT/ws/VNifV2SOAP')
    }

    //tag::validaNifList[]
    List<ConsultaNif> validaNif( List<ConsultaNif> consultaNifList ) {
    //end::validaNifList[]
        def response = client.send() {
            envelopeAttributes(["xmlns:vnif": vnif])
            body {
                consultaNifList.each{ item ->
                    'vnif:VNifV2Ent' {
                        'vnif:Contribuyente'{
                            'vnif:Nif'(item.nif)
                            'vnif:Nombre'(item.nombre.toUpperCase())
                        }
                    }
                }
            }
        }
        response.VNifV2Sal.Contribuyente.each{ result ->
            ConsultaNif nif = consultaNifList.find{ it.nif == "$result.Nif"}
            nif.resultado = "$result.Resultado"
            nif.nombreAeat = "$result.Nombre"
        }
        consultaNifList
    }

    //tag::validaNif[]
    ConsultaNif validaNif( ConsultaNif consultaNif) {
    //end::validaNif[]
        return validaNif([consultaNif]).first()
    }
}
