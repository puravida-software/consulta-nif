package consulta.nif

import groovy.transform.Canonical

@Canonical
class ConsultaNif {

    String nif
    String nombre
    String resultado
    String nombreAeat

}
