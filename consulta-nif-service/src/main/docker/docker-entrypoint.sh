#!/bin/bash
set -e

exec java -Dgrails.env=$SPRING_ENV \
        -jar application.jar \
        --server.port=$APP_PORT \
        --spring.profiles.active=$SPRING_ENV $@
