package consulta.nif.service

import consulta.nif.ConsultaNif
import consulta.nif.ConsultaNifService

class AeatController {

    ConsultaNifService consultaNifService

    def index(){

        ConsultaNif consultaNif = new ConsultaNif(nif:params.nif, nombre:params.nombre)

        respond consultaNifService.validaNif(consultaNif)
    }

}
